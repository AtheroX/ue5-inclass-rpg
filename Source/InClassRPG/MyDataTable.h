// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "MyDataTable.generated.h"

UENUM(BlueprintType)
enum EClases {
	MAGO,
	GUERRERO
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct INCLASSRPG_API FMyDataTable : public FTableRowBase {
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<EClases> myclass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialMp;

	FMyDataTable() : myclass(MAGO), initialHp(50), initialMp(0) {}
};
