// Fill out your copyright notice in the Description page of Project Settings.


#include "En.h"

#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AEn::AEn() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEn::BeginPlay() {
	Super::BeginPlay();

}

void AEn::Hit_Implementation(int dmg) {
	IHittable::Hit_Implementation(dmg);

	hp -= dmg;
	// UE_LOG(LogTemp, Warning, TEXT("Manpegao %s %d/%d"), *this->GetName(), hp, initialHp);
}

// Called every frame
void AEn::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
