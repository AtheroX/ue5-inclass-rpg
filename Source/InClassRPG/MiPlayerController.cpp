// Fill out your copyright notice in the Description page of Project Settings.


#include "MiPlayerController.h"
#include "MyDataTable.h"
#include "En.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

AMiPlayerController::AMiPlayerController() { }

void AMiPlayerController::BeginPlay() {
	Super::BeginPlay();
}

void AMiPlayerController::PlayerTick(float DeltaTime) {
	Super::PlayerTick(DeltaTime);
}


void AMiPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();

	InputComponent->BindAction("Golpea", IE_Pressed, this, &AMiPlayerController::HitAll);
	InputComponent->BindAction("GetClass", IE_Pressed, this, &AMiPlayerController::GetClass);

	InputComponent->BindAxis("Andar", this, &AMiPlayerController::Andar);
	InputComponent->BindAxis("Correr", this, &AMiPlayerController::Correr);
}

void AMiPlayerController::Andar(float axis) {
	if (axis != 0.f)
		XSpeed = 120.f;
	else 
		if(!run)
			XSpeed = 0.f;
	

}

void AMiPlayerController::Correr(float axis) {
	if(axis == 0.f)
		run = false;
	else {
		run =true;
		XSpeed = 900.f;
	}
}

void AMiPlayerController::HitAll() {
	evOnHitAll.Broadcast();

	TArray<AActor*> OutAct;
	UGameplayStatics::GetAllActorsWithInterface(this, UHittable::StaticClass(), OutAct);

	for (AActor* act : OutAct) {
		IHittable* hitto = Cast<IHittable>(act);
		if (hitto) {
			hitto->Hit_Implementation(2);
			UE_LOG(LogTemp, Warning, TEXT("Hitto %d/%d"), hitto->hp, hitto->initialHp);
		}
	}
}

void AMiPlayerController::GetClass() {
	if (ClassDB && !miClass && !className.IsNone()) {
		static const FString context = FString("Obtener clase con la C");
		FMyDataTable* clase = ClassDB->FindRow<FMyDataTable>(className, context, true);
		if (clase)
			miClass = clase;
	}
	else {
		if (miClass) {
			UE_LOG(LogTemp, Warning, TEXT("%s"), *UEnum::GetValueAsString(miClass->myclass.GetValue()))
		}
	}
}
