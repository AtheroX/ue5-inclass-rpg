// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Hittable.h"
#include "GameFramework/Pawn.h"
#include "En.generated.h"

UCLASS()
class INCLASSRPG_API AEn : public APawn, public IHittable {
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	virtual void Hit_Implementation(int dmg) override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
