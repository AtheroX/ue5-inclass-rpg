// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "InClassRPGGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class INCLASSRPG_API AInClassRPGGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
