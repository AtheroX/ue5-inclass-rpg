// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyDataTable.h"
#include "Engine/DataTable.h"
#include "GameFramework/PlayerController.h"
#include "MiPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHitAll);

/**
 * 
 */
UCLASS()
class INCLASSRPG_API AMiPlayerController : public APlayerController {
	GENERATED_BODY()

public:
	AMiPlayerController();

	UPROPERTY(BlueprintAssignable)
	FOnHitAll evOnHitAll;

	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite, Category=DATATABLES)
	UDataTable* ClassDB;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
	FName className; 

	FMyDataTable* miClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int XSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool run;
	
	virtual void BeginPlay() override;
	virtual void PlayerTick(float DeltaTime) override;
	void HitAll();
	void GetClass();
	virtual void SetupInputComponent() override;

	void Andar(float axis);
	void Correr(float axis);

};
